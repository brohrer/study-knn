import os
import numpy as np

np.random.seed(645839)


def load_data():
    # https://www.kaggle.com/shivam2503/diamonds
    # column 0 is row number
    # column 7 is price
    # column 2 cut
    col2_labels = ['"Fair"', '"Good"', '"Very Good"', '"Premium"', '"Ideal"']
    # column 3 color
    col3_labels = ['"J"', '"I"', '"H"', '"G"', '"F"', '"E"', '"D"']
    # column 4 clarity
    col4_labels = [
        '"I1"', '"SI2"', '"SI1"', '"VS2"', '"VS1"', '"VVS2"', '"VVS1"', '"IF"']

    data_filename = os.path.join("data", "diamonds.csv")

    with open(data_filename, "rt") as f:
        data_lines = f.readlines()
        # column_labels = data_lines[0].split(",")
        raw_data = []
        for line in data_lines[1:]:
            line_strings = line.split(",")
            line_strings[2] = col2_labels.index(line_strings[2])
            line_strings[3] = col3_labels.index(line_strings[3])
            line_strings[4] = col4_labels.index(line_strings[4])
            line_nums = [float(val) for val in line_strings[1:]]
            raw_data.append(line_nums)
        raw_data = np.array(raw_data)
        features = raw_data[:, [0, 1, 2, 3, 4, 5, 7, 8, 9]]
        labels = raw_data[:, 6]
        return features, labels


def load_weights(n_features):
    weights = []
    data_filename = "feature_weights.csv"
    try:
        with open(data_filename, "rt") as f:
            data_lines = f.readlines()
            for line in data_lines:
                weights.append(float(line))
            weights = np.array(weights)
    except FileNotFoundError:
        # Handle the case where the feature weights file
        # hasn't been created yet
        weights = np.ones(n_features)

    return weights


def prep_data(features, labels):
    n_features = features.shape[0]
    n_pts = n_features
    # n_pts = 10000
    i_features = np.arange(n_features)
    np.random.shuffle(i_features)
    i_keep = i_features[:n_pts]
    keeper_features = features[i_keep, :]
    keeper_labels = labels[i_keep]
    keeper_features[:, 0] = np.log10(keeper_features[:, 0])
    keeper_labels = np.log10(keeper_labels)

    # Split the data into training and testing
    # n_pts = features.shape[0]
    training_fraction = .8
    n_train = int(n_pts * training_fraction)
    straws = np.arange(n_pts)
    np.random.shuffle(straws)
    i_train = straws[:n_train]
    i_test = straws[n_train:]

    unscaled_train_features = keeper_features[i_train, :]
    unscaled_test_features = keeper_features[i_test, :]
    features_mean = np.mean(unscaled_train_features, axis=0)
    features_stddev = np.sqrt(np.var(unscaled_train_features, axis=0))
    epsilon = 1e-3
    train_features = (
        unscaled_train_features - features_mean) / (features_stddev + epsilon)
    test_features = (
        unscaled_test_features - features_mean) / (features_stddev + epsilon)

    train_labels = keeper_labels[i_train]
    test_labels = keeper_labels[i_test]

    return train_features, train_labels, test_features, test_labels


def find_k_nearest(k_count, train_points, test_point, weights):
    """
    Find the distance between the test point and each of the training points.
    Use the Manhattan distance, the sum of differences in each dimension.
    """
    distance = np.sum(
        np.abs(train_points - test_point[np.newaxis, :]) *
        weights[np.newaxis, :],
        axis=1)
    order = np.argsort(distance)
    i_top_k = order[:k_count]
    distance_top_k = distance[i_top_k]
    return i_top_k, distance_top_k


def score_examples(labels, distances, actual):
    prediction = np.average(labels, weights=1/(distances + 1e-6))
    return -np.abs(actual - prediction)


features, labels = load_data()
n_splits = 10
n_features = features.shape[1]
weights = load_weights(n_features)

for k in [1, 3, 5, 7, 9, 11, 13, 15, 17, 21, 25, 29]:
    scores = []
    for i in range(n_splits):
        (train_features, train_labels, test_features, test_labels) = prep_data(
            features, labels)

        n_test = test_labels.size
        total_score = 0
        for i_test in range(n_test):
            i_top, distances = find_k_nearest(
                k, train_features, test_features[i_test, :], weights)
            score = score_examples(
                train_labels[i_top], distances, test_labels[i_test])
            total_score += score
        iter_score = total_score / n_test
        scores.append(iter_score)

    new_score = np.mean(scores)
    print("k =", k, ",", 100 * (10 ** (- new_score) - 1))
