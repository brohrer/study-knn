import numpy as np


np.random.seed(645839)
# https://www.kaggle.com/mainakchaudhuri/penguin-data-set
data_filename = "Penguins_data.csv"

with open(data_filename, "rt") as f:
    data_lines = f.readlines()
    column_labels = data_lines[0].split(",")

    n_penguins = len(data_lines) - 1
    characteristics = np.zeros((n_penguins, 5))
    labels = np.zeros(n_penguins)
    sex_conversion = {"male": 0, "female": 1}
    label_conversion = {
        "Adelie": 0,
        "Chinstrap": 1,
        "Gentoo": 2,
    }
    for i_penguin, line in enumerate(data_lines[1:]):
        line_data = line.split(",")
        labels[i_penguin] = label_conversion[line_data[0].rstrip()]
        numerical_data = [float(x.rstrip()) for x in line_data[2:6]]
        characteristics[i_penguin, :4] = numerical_data
        characteristics[i_penguin, 4] = sex_conversion[line_data[6].rstrip()]

# Split the data into training and testing
training_fraction = .7
n_train = int(n_penguins * training_fraction)
straws = np.arange(n_penguins)
np.random.shuffle(straws)
i_train = straws[:n_train]
i_test = straws[n_train:]

train_characteristics = characteristics[i_train]
test_characteristics = characteristics[i_test]
train_labels = labels[i_train]
test_labels = labels[i_test]


