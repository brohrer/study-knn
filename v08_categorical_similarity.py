import os
import numpy as np

np.random.seed(645839)


def load_data():
    # https://www.kaggle.com/uciml/zoo-animal-classification
    data_filename = os.path.join("data", "zoo.csv")

    with open(data_filename, "rt") as f:
        data_lines = f.readlines()
        # column_labels = data_lines[0].split(",")
        raw_data = []
        for line in data_lines[1:]:
            line_strings = line.split(",")
            line_nums = [float(val) for val in line_strings[1:]]
            raw_data.append(line_nums)
        raw_data = np.array(raw_data)
        features = raw_data[:, :-1]
        labels = raw_data[:, -1]
        return features, labels


def prep_data(features, labels):
    # Split the data into training and testing
    n_pts = features.shape[0]
    training_fraction = .3
    n_train = int(n_pts * training_fraction)
    straws = np.arange(n_pts)
    np.random.shuffle(straws)
    i_train = straws[:n_train]
    i_test = straws[n_train:]

    unscaled_train_features = features[i_train, :]
    unscaled_test_features = features[i_test, :]
    features_mean = np.mean(unscaled_train_features, axis=0)
    features_stddev = np.sqrt(np.var(unscaled_train_features, axis=0))
    epsilon = 1e-3
    train_features = (
        unscaled_train_features - features_mean) / (features_stddev + epsilon)
    test_features = (
        unscaled_test_features - features_mean) / (features_stddev + epsilon)

    train_labels = labels[i_train]
    test_labels = labels[i_test]

    return train_features, train_labels, test_features, test_labels


def find_k_nearest(k_count, train_points, test_point):
    """
    Find the distance between the test point and each of the training points.
    Use the Manhattan distance, the sum of differences in each dimension.
    """
    distance = 1 / np.sum(train_points == test_point[np.newaxis, :], axis=1)
    order = np.argsort(distance)
    i_top_k = order[:k_count]
    distance_top_k = distance[i_top_k]
    return i_top_k, distance_top_k


def score_examples(labels, distances, actual):
    predictions = np.zeros(int(np.max(labels) + 1))
    for label in labels:
        predictions[int(label)] += 1
    max_vote = np.max(predictions)
    predicted_label_index = np.where(predictions == max_vote)[0]
    if predicted_label_index.size > 1:
        credit = .5
    else:
        credit = 1
    if actual in predicted_label_index:
        return credit
    else:
        return 0


features, labels = load_data()

k = 5
n_reps = 100
n_features = features.shape[1]
n_animals = features.shape[0]
scores = []
for i in range(n_reps):
    (train_features, train_labels, test_features, test_labels) = prep_data(
        features, labels)

    n_test = test_labels.size
    total_score = 0
    for i_test in range(n_test):
        i_top, distances = find_k_nearest(
            k, train_features, test_features[i_test, :])
        score = score_examples(
            train_labels[i_top], distances, test_labels[i_test])
        total_score += score
        show_negative = True
        if show_negative and score == 0:
            print()
            print("actual", int(test_labels[i_test]),
                  "  features",
                  [int(x) for x in test_features[i_test, :] * 10])
            for i_match in i_top:
                print(" label", int(train_labels[i_match]),
                      "  features",
                      [int(x) for x in train_features[i_match, :] * 10])

    scores.append(total_score / n_test)

print("mean score", np.mean(scores))
