import os
import numpy as np
import matplotlib.pyplot as plt
import rasterio


geotiff = os.path.join(
    "data", "USGS_one_meter_x46y404_NM_CO_Southern_San_Luis_TL_2015.tif")
with rasterio.open(geotiff) as src:
    h = src.read()
    h = np.maximum(h, 2300)[0, :, :]

# Gather samples to support the model with
n_rows, n_cols = h.shape
resolution = 6
n_samples = 2**resolution
k = np.minimum(n_samples, 31)
i_rows = [int(x) for x in np.linspace(0, n_rows - 1, n_samples)]
i_cols = [int(x) for x in np.linspace(0, n_cols - 1, n_samples)]
xx, yy = np.meshgrid(i_rows, i_cols)
x_samples = xx.ravel()
y_samples = yy.ravel()
xy_samples = np.concatenate(
    (x_samples[:,np.newaxis], y_samples[:, np.newaxis]), axis=1)
h_samples = h[y_samples, x_samples]

# Reconstruct the image using the samples
resolution_recon = 8
n_recon = 2**resolution_recon
x_recon = [int(x) for x in np.linspace(0, n_rows - 1, n_recon)]
y_recon = [int(x) for x in np.linspace(0, n_cols - 1, n_recon)]
h_recon = np.zeros((n_recon, n_recon))

for j_col, x_r in enumerate(x_recon):
    for i_row, y_r in enumerate(y_recon):
        distance = np.sqrt(np.sum(
            (xy_samples - np.array([[x_r, y_r]]))**2, axis=1))
        i_top_k = np.argpartition(distance, k)[:k]
        distance_top_k = distance[i_top_k]
        h_top_k = h_samples[i_top_k]
        h_recon[i_row, j_col] = np.average(
            h_top_k, weights=1 / (distance_top_k + 100) / 100)

show_image = True
if show_image:
    fig = plt.figure()
    ax = fig.gca()
    ax.imshow(
        h_recon,
        cmap="turbo")
    plt.show()
