import numpy as np
import matplotlib.pyplot as plt
import rasterio

with rasterio.open('data/USGS_one_meter_x46y404_NM_CO_Southern_San_Luis_TL_2015.tif') as src:
    h = src.read()
    h = np.maximum(h, 2300)
    show_image = True
    if show_image:
        fig = plt.figure()
        ax = fig.gca()
        ax.imshow(
            h[0, :, :],
            cmap="turbo")
        plt.show()


    print(src.bounds)
    print(h.shape)
    print(np.max(h), np.min(h))
