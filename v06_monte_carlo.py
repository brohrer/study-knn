import numpy as np

np.random.seed(645839)


def load_data():
    # https://www.kaggle.com/mainakchaudhuri/penguin-data-set
    data_filename = "Penguins_data.csv"

    with open(data_filename, "rt") as f:
        data_lines = f.readlines()
        column_labels = data_lines[0].split(",")

        n_penguins = len(data_lines) - 1
        features = np.zeros((n_penguins, 5))
        labels = np.zeros(n_penguins)
        sex_conversion = {"male": 0, "female": 1}
        label_conversion = {
            "Adelie": 0,
            "Chinstrap": 1,
            "Gentoo": 2,
        }
        for i_penguin, line in enumerate(data_lines[1:]):
            line_data = line.split(",")
            labels[i_penguin] = label_conversion[line_data[0].rstrip()]
            numerical_data = [float(x.rstrip()) for x in line_data[2:6]]
            features[i_penguin, :4] = numerical_data
            features[i_penguin, 4] = sex_conversion[line_data[6].rstrip()]

        return n_penguins, features, labels


def prep_data(n_pengiuns, features, labels):
    # Split the data into training and testing
    training_fraction = .1
    n_train = int(n_penguins * training_fraction)
    n_test = n_penguins - n_train
    straws = np.arange(n_penguins)
    np.random.shuffle(straws)
    i_train = straws[:n_train]
    i_test = straws[n_train:]

    unscaled_train_features = features[i_train, :]
    unscaled_test_features = features[i_test, :]
    features_mean = np.mean(unscaled_train_features, axis=0)
    features_stddev = np.sqrt(np.var(unscaled_train_features, axis=0))
    train_features = (unscaled_train_features - features_mean) / features_stddev
    test_features = (unscaled_test_features - features_mean) / features_stddev

    train_labels = labels[i_train]
    test_labels = labels[i_test]

    return train_features, train_labels, test_features, test_labels


def find_k_nearest(k_count, train_points, test_point):
    """
    Find the distance between the test point and each of the training points.
    Use the Manhattan distance, the sum of differences in each dimension.
    """
    distance = np.sum(np.abs(train_points - test_point[np.newaxis, :]), axis=1)
    order = np.argsort(distance)
    i_top_k = order[:k_count]
    distance_top_k = distance[i_top_k]
    return i_top_k, distance_top_k


def score(labels, distances, actual):
    predictions = np.zeros(3)
    for label in labels:
        predictions[int(label)] += 1
    max_vote = np.max(predictions)
    predicted_label_index = np.where(predictions == max_vote)[0]
    if predicted_label_index.size > 1:
        credit = .5
    else:
        credit = 1
    if actual in predicted_label_index:
        return credit
    else:
        return 0


n_penguins, features, labels = load_data()

k = 5
n_reps = 100
scores = []
for i in range(n_reps): 
    (train_features, train_labels, test_features, test_labels) = prep_data(
        n_penguins, features, labels)

    n_test = test_labels.size
    total_score = 0
    for i_test in range(n_test):
        i_top, distances = find_k_nearest(
            k, train_features, test_features[i_test, :])
        total_score += score(
            train_labels[i_top], distances, test_labels[i_test])

    scores.append(total_score / n_test)

print("mean score", np.mean(scores))
